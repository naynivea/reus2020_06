import javax.swing.JOptionPane;

/**
 * 
 */

/**
 * @author Nayara Nivea Gomes Santos
 *
 */
public class Tasques_UD6_03 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int numero = Integer.parseInt(JOptionPane.showInputDialog("Dime un n�mero y te dir� si es primo o no"));
		JOptionPane.showMessageDialog(null, esPrimo(numero));

	}
	public static String esPrimo(int numero) {
		int divisores = 0;
		for(int i = 1; i <= numero; i++) {
			if(numero % i == 0) {
				divisores++;
			}
		}
		if(divisores == 2) {
			return "Es primo";
		} else {
			return "No es primo";
		}
		
	}
}
