import javax.swing.JOptionPane;

/**
 * 
 */

/**
 * @author Nayara Nivea Gomes Santos
 *
 */
public class Tasques_UD6_06 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		boolean seguir = true;
		int num = 0;
		num = Integer.parseInt(JOptionPane.showInputDialog("Entra un n�mero entero positivo"));
		while(seguir) {
			if(num < 0) {
				num = Integer.parseInt(JOptionPane.showInputDialog("N�mero inv�lido, entrar un n�mero positivo y de valor entero"));
			} else {
				seguir = false;
			}
		}
		
		JOptionPane.showMessageDialog(null, comptarCifras(num));
	}
	
	public static int comptarCifras(int num) {
		String text_num = Integer.toString(num);
		int longitud = text_num.length();
		
		return longitud;
	}

}
